# openstack-folio

Installation of Folio on a OpenStack Cloud using folio-ansible Playbooks.

## Requirements

For this script you have to have 
* Access to a Openstack cloud.
* Installed the openstack client tools (python openstackclient)
* loaded a Openstack rc-file and provided credentials
* git CLI installed
* pwgen installed

## Usage

Clone this git repo including submodules:

`git clone --recurse-submodules https://gitlab.lrz.de/bib-public/openstack-folio.git`

then create a defaults file:

`cp .folio_openstack_defaults_example .folio_openstack_defaults`

And edit it to fit your needs.

Install Folio (Example):

`./install_folio_openstack.sh -n honeysuckle`

Installs a virtial machine with name "vm-honeysuckle" and installs Folio on it.

Options:

`./install_folio_openstack.sh -h
Usage:
./install_folio_openstack.sh -n NameOfVM [-s SSHKeytoUse ] [ -i OpenStackImage ] [ -N OpenStackNetwork ]
        [ -e OpenStackExternalNetwork ] [ -f OpenStackFlavor ] [ -S OpenStackSecurityGroup ]
        [ -g AnsibleFolioGroup ] [ -o OkapiSuperuser ] [ -p OkapiPassword ]
        [ -t FolioTenant ] [ -T FolioTenantName ] [ -d FolioTenantDescription ]
        [ -a FolioTenantAdmin ] [ -A FolioTenantAdminPassword ]
        [ -H -c CertificateFile -k CertificateKeyFile -u FQDN ]
eXprunge (delete) virtual machine
./install_folio_openstack.sh -n NameOfVM -x
`

* -h Help
* -n Name of the virtual machine, if not given, the host part of the FQDN given by -u
* -s Openstack SSH Key name inserted into the VM
* -i Openstack image. Default is Ubuntu-20.04-focal, which is tested and known to work with folio (Q3-2020)
* -N Openstack internal network name
* -e Openstack external network name
* -f Openstack flavor (size of VM, use min. 20G RAM)
* -S Openstack Security Group (firewall settings)
* -g Ansible-folio group: release, release-core, snapshot oder snapshot-core, see folio-ansible group vars
* -o Name of the Okapi superuser
* -p Password of the Okapi Superuser
* -t Name of the Folio tenant
* -T Full name of the Folio tenant
* -d Description of the tenant
* -a Tenant admin unsername
* -A Tenant admin password
* -H enable https support
* -c local path to the certificate file including the certificate chain (used in nginx)
* -k local path to the certificate secret key file. The key file may not be password protected
* -x delete (eXprunge) the virtuRal machine and volume



#!/bin/bash

# default Parameter

if [ ! -r .folio_openstack_defaults ] ; then
  echo "Defaults file not found."
  echo "Creating one"

  echo '
FOLIO_OS_USER=ubuntu
FOLIO_OS_IMAGE=Ubuntu-20.04-focal
FOLIO_OS_VOLSIZE=40
FOLIO_OS_SECURITYGROUP=sg_ansible_folio
FOLIO_OS_NETWORK=internet
FOLIO_OS_FLAVOR=large
FOLIO_OS_NETWORK_EXTERNAL=internet_pool
FOLIO_OS_SSHKEY=MySSHKeyNameInOpenstack
ANSIBLE_FOLIO_GROUP=release
OKAPI_SUPERUSER=opkapi_superuser
OKAPI_PASSWORD=`pwgen 12`
FOLIO_TENANT="mytenant"
FOLIO_TENANT_NAME="My Organisation Name"
FOLIO_TENANT_DESCRIPTION="Description of my Organisation"
FOLIO_TENANT_ADMIN="${FOLIO_TENANT}_admin"
FOLIO_TENANT_ADMIN_PASSWORD=`pwgen 12`
OPENSTACK_RCFILE=myusername-openrc.sh
FOLIO_LOAD_SAMPLE=false
' > .folio_openstack_defaults
fi

source .folio_openstack_defaults

function show_help {
  echo "Usage:"
  echo "$0 -n NameOfVM [-s SSHKeytoUse ] [ -i OpenStackImage ] [ -l OpenstackVolumeSizeinGB ] [ -N OpenStackNetwork ]"
  echo "        [ -e OpenStackExternalNetwork ] [ -f OpenStackFlavor ] [ -S OpenStackSecurityGroup ]"
  echo "        [ -g AnsibleFolioGroup ] [ -o OkapiSuperuser ] [ -p OkapiPassword ]"
  echo "        [ -t FolioTenant ] [ -T FolioTenantName ] [ -d FolioTenantDescription ]"
  echo "        [ -E loadSampleData(true/false) ]"
  echo "        [ -a FolioTenantAdmin ] [ -A FolioTenantAdminPassword ]"
  echo "        [ -H -c CertificateFile -k CertificateKeyFile -u FQDN ]"
  echo "eXprunge (delete) virtual machine"
  echo "$0 -n NameOfVM -x"
}

function set_os_user {
  case "$FOLIO_OS_IMAGE" in
    *buntu* )
       FOLIO_OS_USER=ubuntu
       ;;
    *ebian* )
       FOLIO_OS_USER=debian
       ;;
  esac
}


while getopts "xhHn:i:s:N:f:e:S:g:o:p:t:T:d:c:k:u:a:A:l:E:" opt ; do
  case "$opt" in
  h)
    show_help
    exit 0
    ;;
  n)
    NAME=$OPTARG
    ;;
  x)
    DELETE_VM=1
    ;;
  i)
    FOLIO_OS_IMAGE=$OPTARG
    set_os_user
    ;;
  s)
    FOLIO_OS_SSHKEY=$OPTARG
    ;;
  N)
    FOLIO_OS_NETWORK=$OPTARG
    ;;
  f)
    FOLIO_OS_FLAVOR=$OPTARG
    ;;
  e)
    FOLIO_OS_NETWORK_EXTERNAL=$OPTARG
    ;;
  S)
    FOLIO_OS_SECURITYGROUP=$OPTARG
    ;;
  g)
    ANSIBLE_FOLIO_GROUP=$OPTARG
    ;;
  o)
    OKAPI_SUPERUSER=$OPTARG
    ;;
  p)
    OKAPI_PASSWORD=$OPTARG
    ;;
  t)
    FOLIO_TENANT=$OPTARG
    ;;
  T)
    FOLIO_TENANT_NAME=$OPTARG
    ;;
  d)
    FOLIO_TENANT_DESCRIPTION=$OPTARG
    ;;
  a)
    FOLIO_TENANT_ADMIN=$OPTARG
    ;;
  A)
    FOLIO_TENANT_ADMIN_PASSWORD=$OPTARG
    ;;
  H)
    ENABLE_SSL=1
    ;;
  c)
    CERTIFICATE_FILE=$OPTARG
    ;;
  k)
    CERTIFICATE_KEY_FILE=$OPTARG
    ;;
  u)
    FQDN=$OPTARG
    ;;
  l)
    # Todo: check int
    FOLIO_OS_VOLSIZE=$OPTARG
    ;;
  E)
    # todo: check true/false
    FOLIO_LOAD_SAMPLE=$OPTARG
    ;;
  *)
    echo "Unknown Option -$OPT"
    exit 1
    ;;
  esac
done

if [ -z "$NAME" ] && [ -n "$FQDN" ] ; then
	NAME=${FQDN%%.*}
	echo "Setting Name to $NAME from FQDN $FQDN"
fi


if [ -z "$NAME" ] ; then
      echo "Missing Name (-n) Parameter"
      show_help
      exit 1
fi

if [ -z "$FQDN" ] || [ -z "$CERTIFICATE_FILE" ] || [ -z "$CERTIFICATE_KEY_FILE" ] && [ "$ENABLE_SSL" == 1 ] ; then
	echo "To enable HTTPS, you have to provide FQDN, and Certificate files"
	show_help
	exit 1
fi


function waitvm {
	echo "Waiting for VM to come up"
	# first sleep due to slow reboot
	sleep 10
	while [ "`openstack server show -f value -c status $VMID`" != "ACTIVE" ] ; do
        	echo -n "."
        	sleep 1
	done
	echo "Waiting for boot process to complete"
	if [ -z "$VIP" ] ; then
		sleep 15
	else
		until nc -w 1 $VIP 22 > /dev/null  ; do
			echo -n "*"
		done
	fi
	sleep 10
	echo
}


if [ -z "$OS_PASSWORD" ] ; then
  source $OPENSTACK_RCFILE
fi


if [ "$DELETE_VM" == 1 ] ; then
  echo "Delteting VM $NAME and volume vol_$NAME in 10 seconds!"
  sleep 10
  openstack server delete $NAME
  while openstack server show $NAME > /dev/null 2>&1 ; do
    echo -n "X"
  done
  openstack volume delete vol_$NAME
  exit 0
fi


NETWORKID=`openstack network show -f value -c id $FOLIO_OS_NETWORK`
if [ $? != 0 ] ; then echo "Network $FOLIO_OS_NETWORK not found." ; exit 1 ; fi
EXTNETWORKID=`openstack network show -f value -c id $FOLIO_OS_NETWORK_EXTERNAL`
if [ $? != 0 ] ; then echo "Network $FOLIO_OS_NETWORK_EXTERNAL not found." ; exit 1 ; fi

echo "Creating volume vol_$NAME"
VOLUMEID=`openstack volume create --image $FOLIO_OS_IMAGE --size $FOLIO_OS_VOLSIZE -f value -c id vol_$NAME`
if [ $? != 0 ] ; then echo "Error creating volume" ; exit 1 ; fi
echo "Waiting for volume creation"
while [ "`openstack volume show -f value -c status $VOLUMEID`" != "available" ] ; do
	echo -n "."
	sleep 1
done
echo

echo "Creating virtual machine $NAME"
VMID=`openstack server create --flavor $FOLIO_OS_FLAVOR --volume $VOLUMEID --security-group $FOLIO_OS_SECURITYGROUP --key-name $FOLIO_OS_SSHKEY --availability-zone nova --network $FOLIO_OS_NETWORK --wait -f value -c id $NAME`
if [ $? != 0 ] ; then echo "Error creating virtual machine" ; exit 1 ; fi
waitvm

echo "Binding floating IP to the VM"

# if a FQDN is given, search IP
if [ ! -z "$FQDN" ] ; then
	PREFERRED_IP=`dig +short $FQDN`
	FIPID=`openstack floating ip show -f value -c id $PREFERRED_IP |head -1`
fi

if [ -z "$FIPID" ] ; then
	# search free floating IP
	FIPID=`openstack floating ip list --network $EXTNETWORKID --status DOWN -f value -c ID |head -1`
fi

if [ $? != 0 ] ; then 
	# keine da, neu machen
	echo "Creating new floating IP"
	FIPID=`openstack floating ip create --network $EXTNETWORKID`
	if [ $? != 0 ] ; then
		echo "Could not create floating IP"
		exit 1
	fi
fi
# IP zur ID suchen
FIP=`openstack floating ip show -f value -c 'floating_ip_address' $FIPID`

# IP dem Server zuordnen
openstack server add floating ip $VMID $FIP
if [ $? != 0 ] ; then echo "Error attaching floating IP" ; exit 1 ; fi

sleep 3

echo "Updating operating system of the VM and installing python"
# alte ssh-keys raushauen
ssh-keygen -R $FIP
# Paket updates einspielen und neuen Hostkey akzeptieren
ssh -o "StrictHostKeyChecking=accept-new" ${FOLIO_OS_USER}@$FIP "sudo apt-get -qy update; sudo apt-get -qy upgrade"
# Python installieren - eigentlich ist ein Python3 da, aber ansible findet das nicht (evtl. auch anders lösbar?)
ssh ${FOLIO_OS_USER}@$FIP "sudo apt-get -qy install python python3-psycopg2 python3-pip"

echo "Rebooting"
ssh ${FOLIO_OS_USER}@$FIP "sudo reboot"
waitvm


echo "Creating inventory for folio-ansible playbook"

echo "all:
  hosts:
    $FIP:
      superuser_username: \"$OKAPI_SUPERUSER\"
      superuser_password: \"$OKAPI_PASSWORD\"
      admin_user:
        username: \"$FOLIO_TENANT_ADMIN\"
        password: \"$FOLIO_TENANT_ADMIN_PASSWORD\"
      tenant_parameters:
        - name: loadReference
          value: \"true\"
        - name: loadSample
          value: \"$FOLIO_LOAD_SAMPLE\"
  children:
    ${ANSIBLE_FOLIO_GROUP}:
      hosts:
        $FIP:
          save_dir: /etc/folio/savedir
          okapi_interface: ens3
          okapi_storage: postgres
          tenant: $FOLIO_TENANT
          tenant_name: \"$FOLIO_TENANT_NAME\"
          tenant_description: \"$FOLIO_TENANT_DESCRIPTION\"
          okapi_role: dev
    stripes:
      hosts:
        $FIP:
          stripes_host_address: 0.0.0.0
          stripes_tenant: $FOLIO_TENANT" > inventory_$NAME

if [ "$ENABLE_SSL" == 1 ] ; then
echo "          stripes_enable_https: yes
          stripes_certificate_file: $CERTIFICATE_FILE
          stripes_certificate_key_file: $CERTIFICATE_KEY_FILE
          nginx_proxy_okapi: yes
          stripes_listen_port: 443
          stripes_server_name: 
            - $FQDN
          stripes_okapi_url: https://${FQDN}/okapi/
" >> inventory_$NAME
else
echo "        stripes_okapi_url: http://${FIP}:9130
" >> inventory_$NAME
fi
echo "    production:
      hosts:
        $FIP:" >> inventory_$NAME

# export ANSIBLE_ROLES_PATH=roles_overlay:~/.ansible/roles:/usr/share/ansible/roles:/etc/ansible/roles
export ANSIBLE_PIPELINIG=True

echo "Fixing /etc/hosts"
until ansible-playbook -u ${FOLIO_OS_USER} -i inventory_$NAME fix_hosts.yml ; do
	echo "problem running playbook. Retry in 10 Seconds"
	sleep 10
done

echo "Starting folio playbook"
until ansible-playbook -u ${FOLIO_OS_USER} -i inventory_$NAME folio.yml ; do
	echo "problem running playbook. Retry in 10 Seconds"
        sleep 10
done
cd ..




